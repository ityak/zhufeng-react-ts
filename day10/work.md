## 15. 服务器部署步骤

1. 购买服务器
2. 配置服务器应用环境
3. 安装配置服务器
4. 项目远程部署和发布与更新

### 15.1 购买阿里云服务器

- [选择配置](https://ecs-buy.aliyun.com/wizard/#/postpay/cn-beijing)
- 镜像选择`Ubuntu 16.04 64 位`

![1.buyservver](https://img.zhufengpeixun.com/1.buyservver.png)

![2.centos7.6.png](https://img.zhufengpeixun.com/2.%20centos7.6.png)

![3.password.png](https://img.zhufengpeixun.com/3.password.png)

![4.browserlist.png](https://img.zhufengpeixun.com/4.browserlist.png)

### 15.2 连接服务器

- 连接服务器可以使用 git bash 也可以使用 xshell(只有 windows 有)
- 打开 `git bash`

```js
ssh root@47.92.243.226
```

### 15.3 配置服务器

#### 15.3.2 升级系统

- 每个 LINUX 的发行版，比如 ubuntu，都会维护一个自己的软件仓库
- 在 ubuntu 下，我们维护一个源列表，源列表里面都是一些网址信息，这每一条网址就是一个源，这个地址指向的数据标识着这台源服务器上有哪些软件可以安装使用

```js
//这个命令，会访问源列表里的每个网址，并读取软件列表，然后保存在本地电脑。我们在软件包管理器里看到的软件列表，都是通过update命令更新的
apt-get update
//这个命令，会把本地已安装的软件，与刚下载的软件列表里对应软件进行对比，如果发现已安装的软件版本太低，就会提示你更新。
apt-get upgrade -y
```

#### 15.3.3 安装工具包

```js
apt-get install wget curl git -y
```

#### 15.3.4 安装 node

- [nvm](https://github.com/creationix/nvm/blob/master/README.md)

```js
//下载脚本并执行
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
//添加至环境变量
source  /root/.bashrc
//安装node稳定版
nvm install stable
node -v
npm i cnpm -g
cnpm i pm2 -g
```

#### 15.3.5 nginx

- Nginx 是一个高性能的 HTTP 和反向代理服务器,在本实例中是用来用作静态文件服务器和跨域代理的

##### 15.3.5.1 安装

```js
apt-get install nginx -y
```

##### 15.3.5.2 nginx 命令

| 名称         | 命令                                                        |
| :----------- | :---------------------------------------------------------- |
| 启动 nginx   | nginx -c /etc/nginx/nginx.conf                              |
| 关闭 nginx   | nginx -s stop                                               |
| 重读配置文件 | nginx -s reload kill -HUP nginx                             |
| 常用命令     | service nginx {start/stop/status/restart/reload/configtest} |

#### 15.3.6 mongodb

安装 mongodb

```js
apt-get install mongodb -y
```

### 15.4 部署项目

#### 15.4.1 部署后端项目

##### 15.4.1.1 克隆项目

```js
git clone https://gitee.com/用户名/server.git
```

##### 15.4.1.2 安装并启动

```js
cd server
cnpm install
cnpm run build          //此命令会把ts编译成es5并写入到dist目录中
```

```js
cd dist
```

- 在 dist 目录里添加配置文件 `.env`
- 具体内容根据可以根据实际情况配置

```js
JWT_SECRET_KEY=zhufeng
MONGODB_URL=mongodb://localhost:27017/zhufengketang
PORT=9999
DOMAIN=http://47.93.7.188
```

- 使用 pm2 启动后端接口

```js
pm2 start index.js --name "zhufengketangapi"
```

- 测试接口是否布署成功

```js
curl http://localhost:9999
{"success":true,"message":"hello world"}
```

#### 15.4.2 部署前端项目

##### 15.4.2.1 克隆项目

```sql
git clone https://gitee.com/用户名/client.git
```

##### 15.4.2.2 编译静态文件

- 进入客户端目录安装依赖包并进行编译
- 编译成或后 `dist` 目录里就前台的静态文件了
- 可以建个目录把 `dist` 目录里的文件拷贝过去

```js
cd client
cnpm install
npm run build
mkdir -p /var/www/zhufengketang
cp -r dist/* /var/www/zhufengketang/
```

- 需要注意的是这个编译对服务器内存是有要求的,如果编译失败可以尝试增加服务器的内存

### 15.5 配置 nginx

- nginx 默认配置文件在 `/etc/nginx/nginx.conf`
- 有两个关键配置

```js
include /etc/nginx/conf.d/*.conf;  //包含其它nginx配置文件
include /etc/nginx/sites-enabled/*;  //添加其它站点配置
```

- 进入 `sites-enabled` 目录并且添加一个配置文件`zhufengketang`

```js
cd /etc/nginx/sites-enabled
cp default zhufengketang
```

配置文件`zhufengketang`内容如下

```js
server {
        listen 80; #监听的端口号
        server_name 47.93.7.188; #服务器的IP或者域名,如果使用域名的话需要备案

        root /var/www/zhufengketang; # 静态文件根目录,去哪个目录里找静态文件

        index index.html index.htm index.nginx-debian.html;# 默认文件名

        location / {
                 try_files $uri $uri/ /index.html;
        }
        location ~ \/(user|slider|lesson|uploads) { #t重定向后端接口
                proxy_pass http://127.0.0.1:9999;
        }
}
```

然后重新加载 nginx 配置文件

```js
nginx -s reload
```

然后布署就完成了,就可以在浏览器打开此应用了

```js
http://47.93.7.188/
```