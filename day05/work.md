## 6.个人中心

- 本章主要编写`Profile`组件,就是切换到个人中心页的时候,先发起一个 ajax 请求判断此用户是否登录,如果已经登录的话显示用户信息,如果未登录的请提示跳转到登录和注册页
- 本章实践的内容
  - 路由的切换
  - 如何在 hooks 中发起 ajax 请求
  - 如何保存及发送时携带 token
  - 如何根据环境不同加载不同的接口地址
  - 如何编写`axios`拦截器

本章目录

```js
├── package.json
├── src
│   ├── api
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   └── HomeHeader
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   └── Profile
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mine.tsx
│   │       └── profile.tsx
│   └── typings
│       ├── images.d.ts
│       └── login-types.tsx
├── tsconfig.json
└── webpack.config.js
```

本章效果

![profileroute](https://img.zhufengpeixun.com/profileroute.gif)

### 6.1 Profile\index.tsx

src\routes\Profile\index.tsx

```js
import React, { PropsWithChildren, useEffect } from "react";
import { connect } from "react-redux";
import { CombinedState } from "../../store/reducers";
import { ProfileState } from "../../store/reducers/profile";
import actions from "../../store/actions/profile";
import LOGIN_TYPES from "../../typings/login-types";
import { RouteComponentProps } from "react-router";
import { Descriptions, Button, Alert, message } from "antd";
import NavHeader from "../../components/NavHeader";
import { AxiosError } from "axios";
import "./index.less";
//当前的组件有三个属性来源
//1.mapStateToProps的返回值 2.actions对象类型 3. 来自路由 4.用户传入进来的其它属性
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type RouteProps = RouteComponentProps<Params>;
type Props = PropsWithChildren<StateProps & DispatchProps & RouteProps>;
function Profile(props: Props) {
  //组件加载后直接 发起验证请求,查看此用户是否已经登录过了,如果没有登录则提示错误
  useEffect(() => {
    props.validate().catch((error: AxiosError) => message.error(error.message));
  }, []);
  let content; //里存放着要渲染的内容
  if (props.loginState == LOGIN_TYPES.UN_VALIDATE) {
    //如果未验证则内容为null
    content = null;
  } else if (props.loginState == LOGIN_TYPES.LOGINED) {
    //如果已经登录显示用户信息
    content = (
      <div className="user-info">
        <Descriptions title="当前登录用户">
          <Descriptions.Item label="用户名">珠峰架构</Descriptions.Item>
          <Descriptions.Item label="手机号">15718856132</Descriptions.Item>
          <Descriptions.Item label="邮箱">zhangsan@qq.com</Descriptions.Item>
        </Descriptions>
        <Button type="primary">退出登录</Button>
      </div>
    );
  } else {
    //如果没有登录,则显示注册和登录按钮
    content = (
      <>
        <Alert
          type="warning"
          message="当前未登录"
          description="亲爱的用户你好，你当前尚未登录，请你选择注册或者登录"
        />
        <div style={{ textAlign: "center", padding: "50px" }}>
          <Button type="dashed" onClick={() => props.history.push("/login")}>
            登录
          </Button>
          <Button
            type="dashed"
            style={{ marginLeft: "50px" }}
            onClick={() => props.history.push("/register")}
          >
            注册
          </Button>
        </div>
      </>
    );
  }
  return (
    <section>
      <NavHeader history={props.history}>个人中心</NavHeader>
      {content}
    </section>
  );
}

let mapStateToProps = (state: CombinedState): ProfileState => state.profile;
export default connect(mapStateToProps, actions)(Profile);
```

### 6.2 routes\Profile\index.less

src\routes\Profile\index.less

```less
.user-info {
  padding: 20px;
}
```

### 6.3 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = 'ADD';
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = 'SET_CURRENT_CATEGORY';
//发起验证用户是否登录的请求
+export const VALIDATE = 'VALIDATE';
```

### 6.4 typings\login-types.tsx

src\typings\login-types.tsx

```js
enum LOGIN_TYPES {
    UN_VALIDATE, //未验证过
    LOGINED,     //登录
    UNLOGIN      //未登录
}
export default LOGIN_TYPES;
```

### 6.5 reducers\profile.tsx

src\store\reducers\profile.tsx

```js
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import LOGIN_TYPES from "../../typings/login-types";
export interface ProfileState {
  loginState: LOGIN_TYPES; //当前用户的登录状态
  user: any; //当前已经登录的用户信息
  error: string | null; //错误信息
}
let initialState: ProfileState = {
  //初始状态
  loginState: LOGIN_TYPES.UN_VALIDATE, //当前用户的登录状态
  user: null, //当前已经登录的用户信息
  error: null, //错误信息
};
export default function (
  state: ProfileState = initialState,
  action: AnyAction
): ProfileState {
  switch (action.type) {
    case TYPES.VALIDATE:
      if (action.payload.success) {
        //如果此用户已经登录了
        return {
          ...state,
          loginState: LOGIN_TYPES.LOGINED,
          user: action.payload.data, //设置用户名
          error: null, //没有错误
        };
      } else {
        return {
          ...state,
          loginState: LOGIN_TYPES.UNLOGIN,
          user: null, //用户名为空
          error: action.payload, //错误对象赋值
        };
      }
    default:
      return state;
  }
}
```

### 6.6 actions\profile.tsx

src\store\actions\profile.tsx

```js
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import { validate } from "../../api/profile";
export default {
  //https://github.com/redux-utilities/redux-promise/blob/master/src/index.js
  validate(): AnyAction {
    //发起判断当前用户是否登录的请求
    return {
      type: TYPES.VALIDATE,
      payload: validate(),
    };
  },
};
```

### 6.7 api\index.tsx

src\api\index.tsx

```js
import axios from "axios";
//设置AJAX请求的基准路径,如果是开发环境就指向 http://localhost:8000
axios.defaults.baseURL =
  process.env.NODE_ENV === "production" ? "/" : "http://localhost:8000";
//设置请求体类型为 application/json
axios.defaults.headers.post["Content-Type"] = "application/json;charset=UTF-8";
axios.interceptors.request.use(
  (config) => {
    //在发送请求前把sessionStorage中的token写到请求头里
    let access_token = sessionStorage.getItem("access_token");
    config.headers = {
      Authorization: `Bearer ${access_token}`,
    };
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  (response) => response.data,
  (error) => Promise.reject(error)
);
export default axios;
```

### 6.9 src\api\profile.tsx

src\api\profile.tsx

```js
import axios from "./index";
export function validate() {
  return axios.get("/user/validate");
}
```

### 6.10 NavHeader\index.tsx

src\components\NavHeader\index.tsx

```js
import React from "react";
import "./index.less";
import { LeftOutlined } from "@ant-design/icons";
interface Props {
  history: any;
  children: any;
}
export default function NavHeader(props: Props) {
  return (
    <div className="nav-header">
      <LeftOutlined onClick={() => props.history.goBack()} />
      {props.children}
    </div>
  );
}
```

### 6.11 NavHeader\index.less

src\components\NavHeader\index.less

```less
.nav-header {
  position: fixed;
  left: 0;
  top: 0;
  height: 100px;
  z-index: 1000;
  width: 100%;
  box-sizing: border-box;
  text-align: center;
  line-height: 100px;
  background-color: #2a2a2a;
  color: #fff;
  i {
    position: absolute;
    left: 20px;
    line-height: 100px;
  }
}
```
