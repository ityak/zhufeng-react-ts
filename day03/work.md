## 3.实现首页头部导航

- [类型声明](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/index.d.ts)
- 本章我们将要实现首页的头部导航
- 本章我们要掌握的知识点
  - 配置 ts 能识别的路径别名
  - 如何在 ts 中导入图片
  - React 动画库的使用
  - 如何创建 redux 仓库以及如何关联组件

本章代码

```js
├── package.json
├── src
│   ├── assets
│   │   └── css
│   │       └── common.less
│   ├── components
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   └── Profile
│   │       └── index.tsx
│   └── store
│       ├── action-types.tsx
│       ├── history.tsx
│       ├── index.tsx
│       └── reducers
│           ├── home.tsx
│           ├── index.tsx
│           ├── mine.tsx
│           └── profile.tsx
├── tsconfig.json
└── webpack.config.js
```

本章效果预览
![homenavigation](https://img.zhufengpeixun.com/homenavigation.gif)

### 3.1 tsconfig.json

- 使用 typescript 开发项目时，为了提高开发体验，经常会配置`paths`映射模块路径

tsconfig.json

```json
  "compilerOptions": {
    "baseUrl": ".",  // 解析非相对模块的基地址，默认是当前目录
    "paths": {       // 路径映射，相对于baseUrl
      "@/*": ["./src/*" ] //把@映射为src目录
    }
  },
```

### 3.2 HomeHeader\index.tsx

src\routes\Home\components\HomeHeader\index.tsx

```js
import React, { useState, CSSProperties } from 'react';
import {BarsOutlined} from '@ant-design/icons';
import classnames from 'classnames';
import { Transition } from 'react-transition-group';
//ts默认不支持png格式,需要添加images.d.ts声明文件以支持加载png
import logo from '@/assets/images/logo.png';
import './index.less';
const duration = 1000;
//默认样式
const defaultStyle = {
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: 0,
}
interface TransitionStyles {
    entering: CSSProperties;//进入时的样式
    entered: CSSProperties;//进入成功时的样式
    exiting: CSSProperties;//退出时的样式
    exited: CSSProperties;//退出成功时的样式
}
const transitionStyles: TransitionStyles = {
    entering: { opacity: 1 },//不透明度为1
    entered: { opacity: 1 }, //不透明度为1
    exiting: { opacity: 0 }, //不透明度为0
    exited: { opacity: 0 },  //不透明度为0
};


interface Props {
    currentCategory: string;//当前选中的分类 此数据会放在redux仓库中
    setCurrentCategory: (currentCategory: string) => any;// 改变仓库中的分类
}
function HomeHeader(props: Props) {
    let [isMenuVisible, setIsMenuVisible] = useState(false);//设定标识位表示菜单是否显示
    //设置当前分类,把当前选中的分类传递给redux仓库
    const setCurrentCategory = (event: React.MouseEvent<HTMLUListElement>) => {
        let target: HTMLUListElement = event.target as HTMLUListElement;
        let category = target.dataset.category;//获取用户选择的分类名称
        props.setCurrentCategory(category);//设置分类名称
        setIsMenuVisible(false);//关闭分类选择层
    }
    return (
        <header className="home-header">
            <div className="logo-header">
                <img src={logo} />
                <BarsOutlined onClick={() => setIsMenuVisible(!isMenuVisible)} />
            </div>
            <Transition in={isMenuVisible} timeout={duration}>
                {
                    (state: keyof TransitionStyles) => (
                        <ul
                            className="category"
                            onClick={setCurrentCategory}
                            style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}
                        >
                            <li data-category="all" className={classnames({ active: props.currentCategory === 'all' })}>全部课程</li>
                            <li data-category="react" className={classnames({ active: props.currentCategory === 'react' })}>React课程</li>
                            <li data-category="vue" className={classnames({ active: props.currentCategory === 'vue' })}>Vue课程</li>
                        </ul>
                    )
                }
            </Transition>
        </header>
    )
}
export default HomeHeader;
```

### 3.3 HomeHeader\index.less

src\routes\Home\components\HomeHeader\index.less

```less
@BG: #2a2a2a;
.home-header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 999;
  .logo-header {
    height: 100px;
    background: @BG;
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
    img {
      width: 200px;
      margin-left: 20px;
    }
    span.anticon.anticon-bars {
      font-size: 60px;
      margin-right: 20px;
    }
  }
  .category {
    position: absolute;
    width: 100%;
    top: 100px;
    left: 0;
    padding: 10px 50px;
    background: @BG;
    li {
      line-height: 60px;
      text-align: center;
      color: #fff;
      font-size: 30px;
      border-top: 1px solid lighten(@BG, 20%);
      &.active {
        color: red;
      }
    }
  }
}
```

### 3.4 action-types.tsx

src\store\action-types.tsx

```diff
//设置当前分类的名称
+export const SET_CURRENT_CATEGORY = 'SET_CURRENT_CATEGORY';
```

### 3.5 reducers\home.tsx

src\store\reducers\home.tsx

```diff
import { AnyAction } from 'redux';
+import * as TYPES from "../action-types";
export interface HomeState {
+    currentCategory: string;
}
let initialState: HomeState = {
+    currentCategory: 'all'//默认当前的分类是显示全部类型的课程
};
export default function (state: HomeState = initialState, action: AnyAction): HomeState {
    switch (action.type) {
+        case TYPES.SET_CURRENT_CATEGORY://修改当前分类
+            return { ...state, currentCategory: action.payload };
        default:
            return state;
    }
}
```

### 3.6 actions\home.tsx

src\store\actions\home.tsx

```js
import * as TYPES from "../action-types";
export default {
  setCurrentCategory(currentCategory: string) {
    return { type: TYPES.SET_CURRENT_CATEGORY, payload: currentCategory };
  },
};
```

### 3.7 Home\index.tsx

src\routes\Home\index.tsx

```diff
import React, { PropsWithChildren } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
+import actions from '@/store/actions/home';
+import HomeHeader from './components/HomeHeader';
+import { CombinedState } from '@/store/reducers';
+import { HomeState } from '@/store/reducers/home';
+import './index.less';
+type StateProps = ReturnType<typeof mapStateToProps>;
+type DispatchProps = typeof actions;
+interface Params { }
+type Props = PropsWithChildren<RouteComponentProps<Params> & StateProps & DispatchProps>;
+function Home(props: Props) {
+    return (
+        <>
+            <HomeHeader
+                currentCategory={props.currentCategory}
+                setCurrentCategory={props.setCurrentCategory}
+            />
+        </>
+    )
+}
+let mapStateToProps = (state: CombinedState): HomeState => state.home;
+export default connect(
+    mapStateToProps,
+    actions
+)(Home);
```
视频：[珠峰课堂-首页头部导航](https://img.zhufengpeixun.com/2.%E7%8F%A0%E5%B3%B0%E8%AF%BE%E5%A0%82-%E9%A6%96%E9%A1%B5%E5%A4%B4%E9%83%A8%E5%AF%BC%E8%88%AA.mp4)